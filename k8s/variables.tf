variable "username" {
  default = "admin"
}

variable "host" {}
variable client_certificate {}
variable client_key {}
variable cluster_ca_certificate {}