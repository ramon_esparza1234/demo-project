resource "kubernetes_service" "jenkins_master" {
    metadata {
        name = "jenkins-master"
        labels {
            app = "jenkins"
            role = "master"
        }
    }
    spec {
        selector {
            app = "${kubernetes_pod.jenkins_master.metadata.0.labels.app}"
            role = "master"
        }
        
        port {
            port = 8080
            target_port = 8080
        }
        type = "LoadBalancer"
    }
}
