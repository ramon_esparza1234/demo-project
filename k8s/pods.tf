resource "kubernetes_pod" "jenkins_master" {
    metadata {
        name = "jenkins-master"
        
        labels {
            app = "jenkins"
            role = "master"
        }
    }
    spec {
        container {
            image = "jenkins"
            name  = "jenkins"

            port {
                container_port = 8080
            }
        }
    }
}


