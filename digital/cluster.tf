resource "digitalocean_kubernetes_cluster" "test" {
    name = "test"
    region = "nyc1"
    version = "1.12.1-do.2"

    node_pool {
    name       = "foobar"
    size       = "s-1vcpu-2gb"
    node_count = 1
    tags       = ["one", "two"] // Tags from cluster are automatically added to node pools
  }
}
