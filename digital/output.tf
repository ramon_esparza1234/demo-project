output "client_certificate" {
  value     = "${digitalocean_kubernetes_cluster.test.kube_config.0.client_certificate}"
  sensitive = true
}

output "client_key" {
  value     = "${digitalocean_kubernetes_cluster.test.kube_config.0.client_key}"
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = "${digitalocean_kubernetes_cluster.test.kube_config.0.cluster_ca_certificate}"
  sensitive = true
}

output "host" {
  value     = "${digitalocean_kubernetes_cluster.test.endpoint}"
  sensitive = true
}
