module "digital_ocean" {
    source = "./digital"
}

module "kubernetes" {
    source = "./k8s"
    host     = "${module.digital_ocean.host}"

    client_certificate     = "${module.digital_ocean.client_certificate}"
    client_key             = "${module.digital_ocean.client_key}"
    cluster_ca_certificate = "${module.digital_ocean.cluster_ca_certificate}"
}
